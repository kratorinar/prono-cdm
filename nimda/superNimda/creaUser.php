<h1>Gestion des Utilisateurs</h1>
<section class="user">
    <?php
    $action = $_GET['action'];
    switch ($action) {
        default:
            // Formulaire d'enregistrement d'un nouvel utilisateur dans la BDD
    ?>
            <form action="index.php?page=user&action=create" method="post" class="user-edit">
                <h3>Création d'un utilisateur</h3>
                <label for="username">Nom d'utilisateur</label>
                <input type="text" name="username">
                <label for="mdp">Mot de passe</label>
                <input type="text" name="mdp">
                <button>Enregistrer</button>
            </form>
            <?php
            // On affiche tous les utilisateurs de la base de données pour pouvoir les modifier par la suite
            $requete = "SELECT * FROM user ORDER BY id ASC";
            $s = $cnx->prepare($requete);
            $s->execute();
            $i = 0;
            echo "<form class='user-edit' action='index.php?page=user&action=delete' class='list-user' method='post' >";
            echo "<h3>Modification et Suppression des utilisateurs</h3>";
            echo "<i>Cochez la ou les case(s) pour supprimer(s) un ou des utilisateur(s)</i>";
            while ($r = $s->fetch()) {
                // On crée les liens de modification en récupérant les noms
                $lien = 'index.php?page=user&action=edit&id=' . $r['id'];
                echo '<p>';
                // lien pour aller éditer l'utilisateur
                echo '<a href="' . $lien . '">' . $i . ' - ' . $r['username'] . '</a>';
                // crée un id pour lutilisateur pour la suppression
                echo '<input type="hidden" name="id[' . $i . ']" value="' . $r['id'] . '"/>';
                // checkbox a cocher pour savoir qui supprimer
                echo '<input type="checkbox" name="sup[' . $i . ']" value="1"/>';
                echo '</p>';
                $i++;
            }
            // nombre de lien crée
            echo '<input type="hidden" name="nb" value="' . $i . '">';
            echo '<button>Supprimer</button>';
            echo "</form>";
            break;
        case 'delete':
            echo '<h2>Utilisateur(s) supprimer !</h2>';
            // on boucle sur le nombre de lien crée
            for ($i = 0; $i <= $_POST['nb']; $i++) {
                // si le checkbox sup est cocher alors on la supprime
                if ($_POST['sup'][$i] == 1) {
                    $sup = $cnx->prepare("DELETE FROM user WHERE id=?");
                    $sup->execute([$_POST['id'][$i]]);
                } else {
                    // si l'index existe plus on se redirige vers la page de gestion des utilisateurs
                    header('Location: index.php?page=user');
                }
            }
            break;
        case 'create':
            // Enregistrement d'un utilisateur avec binValue ou bindParam
            $req = "INSERT INTO user(login,mdp) VALUES(:login,:mdp)";
            $ins = $cnx->prepare($req);
            $ins->bindParam(":login", $_POST['login'], PDO::PARAM_STR);
            // Hash du mdp pour la BDD
            $mdp = password_hash($_POST['mdp'], PASSWORD_DEFAULT);
            $ins->bindValue(":mdp", $mdp, PDO::PARAM_STR);
            $ins->execute();
            // on revient sur le formulaire de création d'utilisateur
            header('Location: index.php?page=user');
            break;
        case 'edit':
            echo "<h2>Édition d'un utilisateur</h2>";
            // recuperation des valeur de la base de donnée pour les afficher dans le formualire pour les modifiées
            // on recupère l'id de l'utilisateur dans l'url
            $id = $_GET['id'];
            $req = "SELECT * FROM profil WHERE id=$id ";
            $s = $cnx->prepare($req);
            $s->execute();
            $r = $s->fetch();
            ?>
            <form action="index.php?page=user&action=update&id=<?php echo $_GET['id']; ?>" method="post" class="user-edit">
                <label for="login">Nom d'utilisateur</label>
                <input type="text" name="login" value="<?php echo $r['login']; ?>">

                <label for="mdp">Mot de passe</label>
                <input type="text" name="mdp">

                <button>Enregistrer</button>
            </form>
    <?php
            break;
        case 'update':
            // Mise a jour de la BDD
            $req = "UPDATE profil SET ";
            if (!empty($_POST['mdp'])) {
                $req .= "mdp=:mdp,";
            } else {
                $req .= "";
            }
            $req .= "login=:login WHERE id=:id";
            $updt = $cnx->prepare($req);
            if (!empty($_POST['mdp'])) {
                // on hash le nouveau mdp
                $mdp = password_hash($_POST['mdp'], PASSWORD_DEFAULT);
                $updt->bindValue(":mdp", $mdp, PDO::PARAM_STR);
            }
            $updt->bindParam(":login", $_POST['login'], PDO::PARAM_STR);
            $updt->bindValue(':id', $_GET['id'], PDO::PARAM_INT);
            $updt->execute();
            echo "<h1>Utilisateur mis a jour !</h1><br>";
            echo '<a href="index.php?page=user">Retour</a>';
            break;
    }

    ?>
</section>