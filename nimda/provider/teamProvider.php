<?php
include('entity/getAllResponse.php');
include('entity/groupDO.php');
include('entity/teamDO.php');

class TeamProvider
{
    /**
     * recupere les équipe de l'api est crée un objet
     */
    function getTeams():GetAllResponse
    {
        $team = json_decode(file_get_contents('https://world-cup-json-2022.fly.dev/teams'));
        $getAllResponse = new GetAllResponse;
        $getAllResponse->groups = [];
        foreach ($team->groups as $group) {
            $poule = new GroupDO;
            $poule->letter = $group->letter;
            $poule->teams = [];
            foreach ($group->teams as $value) {
                $team = new TeamDO;
                $team->country = $value->country;
                $team->name = $value->name;
                $team->group_letter = $value->group_letter;
                $team->group_points = $value->group_points;
                $team->wins = $value->wins;
                $team->draws = $value->draws;
                $team->losses = $value->losses;
                $team->games_played = $value->games_played;
                $team->goals_for = $value->goals_for;
                $team->goals_against = $value->goals_against;
                $team->goal_differential = $value->goal_differential;
                array_push($poule->teams, $team);
            }
            array_push($getAllResponse->groups, $poule);
        }
        return $getAllResponse;
    }
}