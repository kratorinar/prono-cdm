<?php
include('../entity/matchDO.php');
include('../entity/teamMatchDO.php');
include('../entity/homeTeamDO.php');
include('../entity/awayTeamDO.php');
/**
 * crée un objet contenant les informations sur les matchs joués
 */
class MatchProvider
{
    /**
     * récupère les matchs de l'api est crée un objet contenant les equipe et les buts pour chaque match
     */
    function getMatch():MatchDO
    {
        $match = json_decode(file_get_contents('https://world-cup-json-2022.fly.dev/matches'));
        $matchDO = new MatchDO;
        $matchDO->match = [];
        foreach ($match as $value) {
            $teamMatch = new TeamMatchDO;
            $teamMatch->stage_name = $value->stage_name;
            $teamMatch->homeTeam = [];
            $teamMatch->awayTeam = [];
            $homeTeam = new HomeTeamDO;
            $awayTeam = new AwayTeamDO;
            $homeTeam->name = $value->home_team->name;
            $homeTeam->goals = $value->home_team->goals;
            $awayTeam->name = $value->away_team->name;
            $awayTeam->goals = $value->away_team->goals;
            array_push($teamMatch->homeTeam, $homeTeam);
            array_push($teamMatch->awayTeam, $awayTeam);
            array_push($matchDO->match, $teamMatch);
        }
        return $matchDO;
    }
    /**
     * object de match trier par groupe
     */
    function getMatchByGroup()
    {
        self::getMatch()->match;

    }
}

$test = new MatchProvider;
$test->getMatch();
