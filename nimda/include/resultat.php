<h2>Résultat</h2>
<?php
require_once('service/teamService.php');
$teamProvider = new TeamProvider;

$getAllResponse = new GetAllResponse;
$getAllResponse = $teamProvider->getTeams();
// var_dump($getAllResponse);

// $teams = json_decode(file_get_contents('https://world-cup-json-2022.fly.dev/teams/'));
// $matches = json_decode(file_get_contents('https://world-cup-json-2022.fly.dev/matches/'));

?>
<style>
    table,
    th,
    td {
        border: 1px solid black;
    }
</style>
<main class="d-flex justify-content-between">
    <section class="classement">
        <h3>Classement</h3>
        <?php
        $teamService = new TeamService;
        $letterGroup = ["A", "B", "C", "D", "E", "F", "G", "H"];
        foreach ($letterGroup as $letter) {
        ?>
            <table class="table table-hover table-striped table-responsive">
                <h3>Poule
                    <?php
                    echo $letter;
                    ?>
                </h3>
                <thead>
                    <th>
                        Équipe
                    </th>
                    <th>
                        Matchs joués
                    </th>
                    <th>
                        Victoire
                    </th>
                    <th>
                        Nul
                    </th>
                    <th>
                        Défaite
                    </th>
                    <th>
                        But marqués
                    </th>
                    <th>
                        But encaissé
                    </th>
                    <th>
                        Différence
                    </th>
                    <th>
                        Total points
                    </th>
                </thead>
                <tbody>
                    <tr>
                        <?php
                        $teamService->teamAllDataGroupTab($letter);
                        ?>
                    </tr>
                </tbody>
            </table>
        <?php
        }
        // $teamService->teamAllDataGroupTab("A");
        ?>
    </section>
    <section>
        <table>
            <h3>Podium</h3>
            <thead>
                <th>
                    test
                </th>
            </thead>
            <tbody>
                <tr>
                    <?php
                    $teamService->rankingTeamGroup("A");
                    ?>
                </tr>
            </tbody>
        </table>
    </section>
</main>