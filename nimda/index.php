<?php include('include/header.php'); ?>
</head>

<body>
    <?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    // On vérifie les connexions permanentes (Cookie)
    if (isset($_COOKIE['login'])) {
        // On vérifie que les cookies correspondent aux données dans la table user
        $cnx = new PDO("mysql:host=localhost;dbname=cms-cours", "root", "");
        $s = $cnx->prepare('SELECT * FROM cms_user WHERE login=? AND mdp=?');
        $s->execute([$_COOKIE['login'], $_COOKIE['mdp']]);
        // Si ce n'est pas le cas, direction login.php
        if ($s->rowCount() == 0) {
            header('Location:login.php');
        }
        // Si c'est le cas, on crée une session
        else {
            $_SESSION['login'] = $_COOKIE['login'];
        }
    }
    // // Si nous sommes connectés, la variable de session a été créée, on affiche Bonjour
    // if (isset($_SESSION['login'])) {
    $page = $_GET['page'];
    switch ($page) {
        default:
    ?>
            <header>
                <nav>
                    <a href="index.php">Pages principal</a>
                    <a href="?page=resultat">Résultat</a>
                    <a href="">Pronostique phase de poule</a>
                    <a href="">Pronostique phase final</a>
                    <a href="">Compte</a>
                    <a href="logout.php">Deconnexion</a>
                </nav>
            </header>
            <?php
            $cnx = new PDO("mysql:host=localhost;dbname=cms-cours", "root", "");
            ?>

    <?php
            break;
        case 'resultat':
            include('include/resultat.php');
            break;
        case 'pronopp':
            # code...
            break;
        case 'pronopf':
            # code...
            break;
        case 'compte':
            # code...
            break;
    }
    // }else {
    //     // Si on n'est pas connecté, on renvoie à la page login
    //     header("Location:login.php");
    // }
    ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    <?php include('include/footer.php'); ?>