<?php
require_once('provider/teamProvider.php');
class TeamService extends TeamProvider
{
    /** Fonction pour afficher toute les datas d'une équipe en phase de poule 
     */
    function teamAllDataGroupTab($groupLetter)
    {
        $group = TeamProvider::getTeams()->groups[self::letterGroup($groupLetter)]->teams;

        foreach ($group as $value) {
            echo "<tr>
                    <td>" . $value->name . "</td>";
            echo "<td>" . $value->games_played . "</td>";
            echo "<td>" . $value->wins . "</td>";
            echo "<td>" . $value->draws . "</td>";
            echo "<td>" . $value->losses . "</td>";
            echo "<td>" . $value->goals_for . "</td>";
            echo "<td>" . $value->goals_against . "</td>";
            echo "<td>" . $value->goal_differential . "</td>";
            echo "<td>" . $value->group_points . "</td>
            </tr>";
        }
    }
    function rankingTeamGroup($groupLetter)
    {
        $group = TeamProvider::getTeams()->groups[self::letterGroup($groupLetter)]->teams;
        $i = 0;
        foreach ($group as $value) {
            $value->group_points = $i++;
            echo "<tr>
                    <td>" . $value->name . "</td>";
            echo "<td>" . $value->group_points . "</td>
            </tr>";
        }
    }
    function letterGroup($letter)
    {
        if ($letter == "A") {
            $letterGroups = 0;
        } else if ($letter == "B") {
            $letterGroups = 1;
        } else if ($letter == "C") {
            $letterGroups = 2;
        } else if ($letter == "D") {
            $letterGroups = 3;
        } else if ($letter == "E") {
            $letterGroups = 4;
        } else if ($letter == "F") {
            $letterGroups = 5;
        } else if ($letter == "G") {
            $letterGroups = 6;
        } else if ($letter == "H") {
            $letterGroups = 7;
        }
        return $letterGroups;
    }
    /**
     * Fonction pour sortir un le nom d'une équipe par rapport a son groupe
     */
    function teamGroupsName($letter, $numTeam)
    {
        $nameTeam = TeamProvider::getTeams()->groups[self::letterGroup($letter)]->teams[$numTeam]->name;
        echo $nameTeam;
    }
    function teamWin($letter, $numTeam)
    {
        $nameTeam = TeamProvider::getTeams()->groups[self::letterGroup($letter)]->teams[$numTeam]->wins;
        echo $nameTeam;
    }
    function teamDraw($letter, $numTeam)
    {
        $nameTeam = TeamProvider::getTeams()->groups[self::letterGroup($letter)]->teams[$numTeam]->draws;
        echo $nameTeam;
    }
    function teamLose($letter, $numTeam)
    {
        $nameTeam = TeamProvider::getTeams()->groups[self::letterGroup($letter)]->teams[$numTeam]->losses;
        echo $nameTeam;
    }
    function teamPlayed($letter, $numTeam)
    {
        $nameTeam = TeamProvider::getTeams()->groups[self::letterGroup($letter)]->teams[$numTeam]->games_played;
        echo $nameTeam;
    }
    function teamGoalScore($letter, $numTeam)
    {
        $nameTeam = TeamProvider::getTeams()->groups[self::letterGroup($letter)]->teams[$numTeam]->goals_for;
        echo $nameTeam;
    }
    function teamGoalConceded($letter, $numTeam)
    {
        $nameTeam = TeamProvider::getTeams()->groups[self::letterGroup($letter)]->teams[$numTeam]->goals_against;
        echo $nameTeam;
    }
    function teamGoalDifferential($letter, $numTeam)
    {
        $nameTeam = TeamProvider::getTeams()->groups[self::letterGroup($letter)]->teams[$numTeam]->goal_differential;
        echo $nameTeam;
    }
    function teamPoints($letter, $numTeam)
    {
        $nameTeam = TeamProvider::getTeams()->groups[self::letterGroup($letter)]->teams[$numTeam]->group_points;
        echo $nameTeam;
    }
};
