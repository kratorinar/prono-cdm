<?php
/**
 * object contenant les matchs joués
 */
class TeamMatchDO
{
    public string $group;
    public string $stage_name;
    public array $homeTeam;
    public array $awayTeam;
}