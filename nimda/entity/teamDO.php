<?php
// object qui reprétente la réponse de l'api
class TeamDO
{
    public string $country;
    public string $name;
    public string $group_letter;
    public ?int $group_points;
    public ?int $wins;
    public ?int $draws;
    public ?int $losses;
    public ?int $games_played;
    public ?int $goals_for;
    public ?int $goals_against;
    public ?int $goal_differential;
}
